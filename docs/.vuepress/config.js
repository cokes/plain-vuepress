// https://vuepress.vuejs.org/config/

module.exports = {
  base: '/plain-vuepress/', // プロジェクトのベースUrl この場合だと example.com/name/ 以下に設置する想定
  title: 'my document',
  description: 'documents library',
  dest: 'public',
  locales: { '/': {lang: 'ja'}
  },
  markdown: {
  extendMarkdown: md => {
    md.set({ breaks: true })
  }},
  themeConfig: {
    nav: [
      { text: 'Home', link: '/' },
      { text: 'About', link: '/about/' },
      { text: 'Blog', link: 'https://www.nxworld.net/' },
        {
          text: 'More',
          items: [
            { text: 'Twitter', link: 'https://twitter.com/' },
            { text: 'GitHub', link: 'https://github.com/' },
            { text: 'Dribbble', link: 'https://dribbble.com/' }
          ]
        }
      ]
    }
}
  /*
  themeConfig: {
    repo: 'https://gitlab.com/cokes/plain-vuepress/',
    docsDir: 'docs',
    nav: [
      {
        text: 'about',
        link: '/about/',
      }
    ]
*/  }
